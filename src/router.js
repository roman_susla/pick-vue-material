import VueRouter from 'vue-router';
import Blank from './components/other/Blank.vue';
import Vendors from './components/vendor/Vendors.vue';
import VendorNew from './components/vendor/VendorNew.vue';
import Vendor from './components/vendor/Vendor.vue';
import VendorInfo from './components/vendor/VendorInfo.vue';
import VendorEdit from './components/vendor/VendorEdit.vue';
import Locations from './components/locations/Locations.vue';
import LocationNew from './components/locations/LocationNew.vue';
import Location from './components/locations/Location.vue';
import LocationInfo from './components/locations/LocationInfo.vue';
import LocationEdit from './components/locations/LocationEdit.vue';
import VendorsSearchToolbar from './components/toolbars/VendorsSearch.vue';
import LocationsSearchToolbar from './components/toolbars/LocationsSearch.vue';
import ReviewSearch from './components/toolbars/ReviewSearch.vue';
import Reviews from './components/review/Reviews.vue';
const router = new VueRouter({
  mode: 'history',
  routes: [
    {path: '/dashboard', component: Blank, name: 'dashboard'},
    {path: '/coupons', component: Blank, name: 'coupons'},
    {path: '/orders', component: Blank, name: 'orders'},
    {path: '/menu', component: Blank, name: 'menu'},
    {path: '/options', component: Blank, name: 'options'},
    {path: '/categories', component: Blank, name: 'categories'},
    {path: '/customers', component: Blank, name: 'customers'},
    {
      path: '/reviews', components: {
      default: Reviews,
      toolbar: ReviewSearch
    }, name: 'reviews'
    },
    {path: '/staff', component: Blank, name: 'staff'},
    {path: '/settings', component: Blank, name: 'settings'},
    {path: '/boxes', component: Blank, name: 'boxes'},
    {
      path: '/vendors/new', components: {
      default: VendorNew,

    },
      name: 'vendors-new'
    },
    {
      path: '/vendors', components: {
      default: Vendors,
      toolbar: VendorsSearchToolbar
    },
      name: 'vendors-list'
    },

    {
      path: '/vendors/:id',
      component: Vendor,
      props: true,
      children: [
        {
          path: '',
          name: 'vendor-info',
          component: VendorInfo
        },
        {
          path: 'edit',
          name: 'vendor-edit',
          components: {
            default: VendorEdit,
          }
        }
      ]
    },
    {
      path: '/locations', components: {
      default: Locations,
      toolbar: LocationsSearchToolbar
    },
      name: 'locations-list'
    },
    {
      path: '/locations/new', components: {
      default: LocationNew,
    },
      name: 'locations-new'
    },
    {
      path: '/locations/:id',
      component: Location,
      props: true,
      children: [
        {
          path: '',
          name: 'location-info',
          component: LocationInfo
        },
        {
          path: 'edit',
          name: 'location-edit',
          components: {
            default: LocationEdit,
          }
        }
      ]
    },
    // { path: '*', component: NotFoundComponent } //404
  ]
});


export default router;
