import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from './components/Main'
import VueMaterial from 'vue-material'
import router from './router.js'
import Vuelidate from 'vuelidate'

import './utils/vue/upper.js'

import "leaflet/dist/leaflet.css";
import 'vue-material/dist/vue-material.min.css'
Vue.use(VueMaterial);
Vue.use(VueRouter);
Vue.use(Vuelidate);
const root = new Vue({
  el: '#app',
  router,
  render: h => h(Main),
  data: {
    expanded: true
  }
});
export default root;
