export default class Item {
  constructor(key, name, path, icon, nested = []) {
    this._key = key;
    this._name = name;
    this._path = path;
    this._icon = icon;
    this._nested = nested;
  }

  get name() {
    return this._name;
  }

  get key() {
    return this._key;
  }

  get path() {
    return this._path;
  }

  get icon() {
    return this._icon;
  }


  get nested() {
    return this._nested;
  }
}
  
  