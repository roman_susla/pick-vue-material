import Vue from 'vue'

Vue.filter('upper', (val) => {
  return val.toUpperCase();
});
