import {EventBus, TITLE} from '../../scripts/eventBus'
export default {
  data: () => ({
    appTitle: 'title'
  }),
  watch: {
    appTitle(to){
      EventBus.$emit(TITLE, to);
    }
  }
}