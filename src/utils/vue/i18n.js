import Vue from 'vue'
import store from '../../store/index.js'

Vue.filter('i18n', (code) => {
  let dictionary = store.state.lang.i18n[store.state.lang.currentLanguage].translated;
  if (dictionary) {
    let value = dictionary[code];
    if (value) {
      return value;
    }
  }
  return code;
});
